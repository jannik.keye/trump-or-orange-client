import 'whatwg-fetch'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const apiUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'https://trump-or-orange-api.herokuapp.com'
const method = 'POST'

const store = new Vuex.Store({
  state: {
    currentFileDataUri: null,
    apiResult: null,
    mode: 'file',
    error: null
  },
  mutations: {
    SET_FILE: (state, payload) => {
      state.currentFileDataUri = payload
    },
    SET_API_RESULT: (state, payload) => {
      state.apiResult = payload
    },
    RESET: state => {
      state.currentFileDataUri = null
      state.apiResult = null
      state.mode = 'file'
    },
    SET_MODE: (state, payload) => {
      state.mode = payload
    },
    SET_ERROR: (state, payload) => {
      state.error = payload
    }
  },
  actions: {
    async handleSendUrl({ commit }, url) {
      commit('SET_ERROR', null)
      commit('SET_FILE', url)

      const res = await fetch(apiUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          url
        })
      })
      const json = await res.json()

      commit('SET_API_RESULT', json)
    },
    setMode({ commit }, payload) {
      commit('SET_MODE', payload)
    },
    reset({ commit }) {
      commit('RESET')
    },
    setFile ({ commit }, input) {
      if (input[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              commit('SET_FILE', e.target.result)
          };

          reader.readAsDataURL(input[0]);
      }
    },
    async handleFileUpload({ commit, dispatch }, { fieldName, fileList}) {
      try {
        const formData = new FormData();

        if (!fileList.length) return;

        dispatch('setFile', fileList)

        Array
          .from(Array(fileList.length).keys())
          .map(x => {
            formData.append(fieldName, fileList[x], fileList[x].name);
          });

        await new Promise(resolve => setTimeout(resolve, 3000))

        const res = await fetch(apiUrl, {
          method: 'POST',
          body: formData
        })
        const json = await res.json()

        commit('SET_API_RESULT', json)
      } catch (error) {
        throw new Error(error)
      }
    }

  },
  getters: {

  }
})

export default store
