const express = require('express')

const app = express()

const port = process.env.PORT || 3000

const path = require('path')

app.use('static', express.static(path.join(path.resolve(__dirname) + '/dist/static')))
app.use('/static/css', express.static(path.resolve(__dirname) + '/dist/static/css'))
app.use('/static/js', express.static(path.resolve(__dirname) + '/dist/static/js'))
app.use('/static/fonts', express.static(path.resolve(__dirname) + '/dist/static/fonts'))
app.get('/*', (req, res) => {
  res.sendFile(path.join(path.resolve(__dirname) + '/dist/index.html'))
})

app.listen(port, () => {
  console.log('App listening on port' + port)
})
